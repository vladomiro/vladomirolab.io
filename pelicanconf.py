#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = 'Vladomiro'
SITENAME = 'Funcional'
SITESUBTITLE= 'Página del curso de programación funcional para física computacional'
SITEURL = 'vladomiro.gitlab.io'

PATH = 'content'

TIMEZONE = 'America/Mexico_City'
DATE_FORMATS = {"es": "%d %b, %Y"}

DEFAULT_LANG = 'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Plugins and extensions
MARKDOWN = {
    "extension_configs": {
        "markdown.extensions.admonition": {},
        "markdown.extensions.codehilite": {"css_class": "highlight"},
        "markdown.extensions.extra": {},
        "markdown.extensions.meta": {},
        "markdown.extensions.toc": {"permalink": " "},
    }
}


SITEMAP = {
    "format": "xml",
    "priorities": {"articles": 0.5, "indexes": 0.5, "pages": 0.5},
    "changefreqs": {"articles": "monthly", "indexes": "daily", "pages": "monthly"},
}

# Appearance
THEME= "/home/devuan/.local/lib/python3.9/site-packages/pelican/themes/elegant"
TYPOGRAFY = True
DEFAULT_PAGINATION = False

# Defaults
DEFAULT_CATEGORY = "Miscellaneous"
USE_FOLDER_AS_CATEGORY = False
ARTICLE_URL = "{slug}"
PAGE_URL = "{slug}"
PAGE_SAVE_AS = "{slug}.html"
TAGS_URL = "tags"
CATEGORIES_URL = "categories"
ARCHIVES_URL = "archives"
SEARCH_URL = "search"


# Social widget
SOCIAL = (('Gitlab', 'https://gitlab.com/vladomiro'),
          ('Fediverso', 'https://fe.disroot.org/@vladomiro'),)

# Elegant theme
STATIC_PATHS = ["theme/images", "images", "extra/_redirects", "code"]
EXTRA_PATH_METADATA = {"extra/_redirects": {"path": "_redirects"}}

if os.environ.get("CONTEXT") == "production":
    STATIC_PATHS.append("extra/robots.txt")
    EXTRA_PATH_METADATA["extra/robots.txt"] = {"path": "robots.txt"}
else:
    STATIC_PATHS.append("extra/robots_deny.txt")
    EXTRA_PATH_METADATA["extra/robots_deny.txt"] = {"path": "robots.txt"}

DIRECT_TEMPLATES = ["index", "tags", "categories", "archives", "search", "404"]
TAG_SAVE_AS = ""
AUTHOR_SAVE_AS = ""
CATEGORY_SAVE_AS = ""
USE_SHORTCUT_ICONS = True

# Landing Page
PROJECTS_TITLE = "Sobre el curso"
PROJECTS = [
    {
        "name": "Notas del curso",
        "url": "https://git.disroot.org/vladomiro/notas-fnys",
        "description": "Notas en latex del curso",
    },
    {
        "name": "Moodle",
        "url": "https://moodle.fciencias.unam.mx",
        "description": "Acceso al moodle de la facultad de ciencias",
    },
    {
        "name": "Libros",
        "url": "https://github.com/Pelican-Elegant/elegant/tree/master/elegant-logo",
        "description": "Libros útiles para el curso",
    },
    {
        "name": "Haskell",
        "url": "https://www.haskell.org/",
        "description": "Más información sobre el lenguaje de programación funcional",
    },
    {
        "name": "Python: funcional",
        "url": "https://docs.python.org/3/howto/functional.html",
        "description": "Algunas cuestiones para convertir python en el paradigma funcional",
    },
    {
        "name": "Replit",
        "url": "https://replit.com/",
        "description": "Plataforma para probar los lenguajes de programación",
    },
    {
        "name": "Sage Math cloud",
        "url": "https://www.sagemath.org/",
        "description": "Una nube especializada en ciencia, en caso de que podamos usarla",
    },
]

LANDING_PAGE_TITLE = "Página del curso de programación funcional para física computacional"


# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
