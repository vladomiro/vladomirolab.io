# Código para la página del curso de programación funcional para la física computacional

Página albergada en GitLab Pages. La página se puede ver en: <https://pages.gitlab.io/pelican>

Más sobre [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/) y la [documentación oficial](https://docs.gitlab.com/ce/user/project/pages/), incluyendo
[como *forkear* un proyecto como este para iniciar](https://docs.gitlab.com/ee/user/project/pages/getting_started_part_two.html#fork-a-project-to-get-started-from).

---

## GitLab CI

La página estática de este proyecto fue construida por [GitLab CI][ci], siguiendo los pasos definidos en el archivo [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Construir localmente

Para trabajar de forma local con este proyecto, sigue los pasos a continuación:

1. *Forkea*, clona o descarga este proyecto
1. [Instala][] Pelican
1. Regenera y monta en el puerto 8000: `make devserver`
1. Agrega el contenido

Lee más en la [documentación](https://getpelican.com/) de Pelican o puedes ver un tutorial en: [https://vladomiro.codeberg.page/](https://vladomiro.codeberg.page/).

## Páginas de usuario o grupo en GitLab Pages

Para utilizar este proyecto como página personal o grupal, debes seguir los siguientes pasos::

1. Renombra tu proyecto como `nombre.gitlab.io`, donde `nombre` es tu `nombre_de_usuario` o `nombre_de_grupo`. Esto se puede hacer navegando a la pestaña de tu proyecto: **Settings > General (Advanced)**.

2. Ajusta la opción de configuración `SITEURL` en `publishconf.py` a la nueva dirección (e.g. `https://nombre.gitlab.io`)

Read more about [GitLab Pages for projects and user/group websites][pagesdoc].

## Use a custom theme

To use a custom theme:

1. Visit <https://github.com/getpelican/pelican-themes> and pick the name of
   the theme you want to use.
1. Uncomment the following lines from `.gitlab-ci.yml`, replacing `<theme_name>`
   with the name of the theme you chose:

   ```yaml
   - svn export https://github.com/getpelican/pelican-themes/trunk/<theme-name> /tmp/<theme-name>
   - pelican-themes --install /tmp/<theme-name>
   ```

1. Edit `pelicanconf.py` and add the theme:

   ```plaintext
   THEME = '/tmp/<theme-name>'
   ```

For more information, see the discussion in [issue #1](https://gitlab.com/pages/pelican/-/issues/1).

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[pelican]: http://blog.getpelican.com/
[install]: https://docs.getpelican.com/en/stable/install.html
[documentation]: http://docs.getpelican.com/
[pagesdoc]: https://docs.gitlab.com/ce/user/project/pages/getting_started_part_one.html
